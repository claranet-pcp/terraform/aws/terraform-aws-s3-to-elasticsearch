import boto3


cloudwatch = boto3.client('cloudwatch')


def put_metric_data(**kwargs):
    """
    Publishes metric data to Amazon CloudWatch.

    """

    response = cloudwatch.put_metric_data(**kwargs)

    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))
