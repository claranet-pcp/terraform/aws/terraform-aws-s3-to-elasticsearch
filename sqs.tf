resource "aws_sqs_queue" "retry" {
  name                      = random_id.name.hex
  message_retention_seconds = 1209600 # maximum value, 14 days
}

