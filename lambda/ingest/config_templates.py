import datetime

try:
    basestring
except NameError:
    basestring = str


class Template(object):

    def __init__(self, index, parser, pipeline, template):
        self._index = index
        self.parser = parser
        self.pipeline = pipeline
        self.template = template

    def get_index(self, timestamp):
        return timestamp.strftime(self._index)


class ValidationError(Exception):

    def __init__(self, template, message):
        self.template = template
        self.message = message

    def __str__(self):
        return '{} in template {}'.format(self.message, self.template)


def build(version, templates):

    if not isinstance(templates, dict):
        raise TypeError('templates must be a dict')

    for name, data in templates.items():

        index = data.get('index')

        if not index:
            raise ValidationError(name, 'index is missing')
        elif not isinstance(index, basestring):
            raise ValidationError(name, 'index is not a string')

        parser = data.get('parser')

        pipeline = data.get('pipeline')
        if pipeline:

            description = pipeline.get('description')
            if not description:
                raise ValidationError(name, 'pipeline.description is missing')
            elif not isinstance(description, basestring):
                raise ValidationError(
                    name, 'pipeline.description must be a list'
                )

            processors = pipeline.get('processors')
            if not processors:
                raise ValidationError(name, 'pipeline.processors is missing')
            elif not isinstance(processors, list):
                raise ValidationError(
                    name, 'pipeline.processors must be a list'
                )

        index_template = data.get('template')
        if index_template:

            if not isinstance(index_template, dict):
                raise ValidationError(name, 'template must be a dict')

            if version == 5:

                index_template_template = index_template.get('template')

                if not index_template_template:
                    raise ValidationError(
                        name, 'template.template is missing'
                    )
                elif not isinstance(index_template_template, basestring):
                    raise ValidationError(
                        name, 'template.template must be a string'
                    )

            else:

                index_patterns = index_template.get('index_patterns')

                if not index_patterns:
                    raise ValidationError(
                        name, 'template.index_patterns is missing'
                    )

            # Convert Terraform boolean strings to real booleans.

            mappings = index_template.get('mappings') or {}
            for mapping in mappings.values():

                properties = mapping.get('properties') or {}
                for options in properties.values():
                    fix_terraform_booleans(options)

                dynamic_templates = mapping.get('dynamic_templates') or []
                for dynamic_template in dynamic_templates:
                    mapping = dynamic_template.get('mapping') or {}
                    fix_terraform_booleans(mapping)

        template = Template(
            index=index,
            parser=parser,
            pipeline=pipeline,
            template=index_template,
        )

        # Check that the index name is a valid date format string.
        template.get_index(datetime.datetime.now())

        yield name, template


def fix_terraform_booleans(data):
    """
    Updates a dictionary to replace Terraform boolean strings with real
    boolean values.

    This is required when configuration comes from Terraform. The Terraform
    jsonencode() function outputs booleans as strings of 0 or 1. Elasticsearch
    does not interpret those strings as booleans, so it must be converted here.

    """

    for key, value in data.items():
        if value == '0':
            data[key] = False
        elif value == '1':
            data[key] = True
