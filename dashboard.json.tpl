{
  "widgets": [
    {
      "type": "metric",
      "x": 6,
      "y": 0,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": false,
        "metrics": [
          [
            "Ingest",
            "Lag",
            "FunctionName",
            "${ingest_function_name}",
            {
              "stat": "Maximum",
              "period": 300
            }
          ]
        ],
        "region": "${region}",
        "title": "Lag",
        "period": 300
      }
    },
    {
      "type": "metric",
      "x": 12,
      "y": 0,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": true,
        "metrics": [
          [
            "Ingest",
            "IndexOperations",
            "FunctionName",
            "${ingest_function_name}",
            {
              "stat": "Sum",
              "label": "Index Operations"
            }
          ]
        ],
        "region": "${region}",
        "title": "Throughput",
        "period": 300
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": true,
        "metrics": [
          [
            "AWS/Lambda",
            "Invocations",
            "FunctionName",
            "${ingest_function_name}",
            {
              "stat": "Sum"
            }
          ],
          [
            ".",
            "Throttles",
            ".",
            ".",
            {
              "stat": "Sum"
            }
          ]
        ],
        "region": "${region}",
        "title": "Executions"
      }
    },
    {
      "type": "metric",
      "x": 12,
      "y": 6,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": true,
        "metrics": [
          [
            "Ingest",
            "RetriedFiles",
            "FunctionName",
            "${retry_function_name}",
            {
              "stat": "Sum",
              "period": 900,
              "label": "Retried Files"
            }
          ],
          [
            ".",
            "FailedFiles",
            ".",
            ".",
            {
              "stat": "Sum",
              "period": 900,
              "label": "Failed Files"
            }
          ]
        ],
        "region": "${region}",
        "title": "File Errors",
        "period": 300
      }
    },
    {
      "type": "metric",
      "x": 6,
      "y": 6,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": true,
        "metrics": [
          [
            "Ingest",
            "InvalidMessages",
            "FunctionName",
            "${retry_function_name}",
            {
              "stat": "Sum",
              "period": 300,
              "label": "Invalid Messages"
            }
          ],
          [
            "AWS/Lambda",
            "DeadLetterErrors",
            ".",
            "${ingest_function_name}",
            {
              "stat": "Sum",
              "period": 300,
              "label": "Dead Letter Errors"
            }
          ],
          [
            ".",
            "Errors",
            ".",
            "${retry_function_name}",
            {
              "stat": "Sum",
              "period": 300,
              "label": "Retry Function Errors"
            }
          ]
        ],
        "region": "${region}",
        "title": "Criticals",
        "period": 300
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 6,
      "width": 6,
      "height": 6,
      "properties": {
        "view": "timeSeries",
        "stacked": true,
        "metrics": [
          [
            "AWS/Lambda",
            "Errors",
            "FunctionName",
            "${ingest_function_name}",
            {
              "stat": "Sum",
              "label": "Ingest Function Errors"
            }
          ]
        ],
        "region": "${region}",
        "title": "Warnings",
        "period": 300
      }
    }
  ]
}