output "lambda_role_arn" {
  value = module.ingest_lambda.role_arn
}

output "lambda_role_name" {
  value = module.ingest_lambda.role_name
}

