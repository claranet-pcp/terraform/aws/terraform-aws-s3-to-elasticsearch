provider "aws" {
  region = "eu-west-1"
}

module "cloudtrail_ingest" {
  source = "../"

  name = "${var.cloudtrail_bucket}-ingest"

  bucket_names = [var.cloudtrail_bucket]
  bucket_count = 1

  elasticsearch_host    = var.elasticsearch_host
  elasticsearch_version = 5

  vpc_enabled            = var.elasticsearch_vpc_enabled
  vpc_subnet_ids         = var.elasticsearch_subnet_ids
  vpc_security_group_ids = var.elasticsearch_security_group_ids

  parsers = [
    {
      template = "aws-cloudtrail"
    },
  ]

  routes = [
    {
      template = "aws-cloudtrail"
    },
  ]
}
