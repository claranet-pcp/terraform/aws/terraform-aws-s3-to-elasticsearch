import itertools
import re

try:
    basestring
except NameError:
    basestring = str


class Route(object):

    def __init__(self, bucket, prefix, template):
        self.bucket = bucket
        self.prefix = prefix
        self.template = template

    def match(self, bucket, key, data):

        if self.bucket:
            if bucket != self.bucket:
                return (False, None)

        if self.prefix:
            if not key.startswith(self.prefix):
                return (False, None)

        return (True, self.template)


class FieldRoute(Route):

    def __init__(self, bucket, prefix, field,
                 value_templates, value_regex_templates):

        self.bucket = bucket
        self.prefix = prefix

        if isinstance(field, list):
            self.field = field
        else:
            self.field = [field]

        self.value_templates = value_templates

        self.value_regex_templates = []
        for pattern, template in value_regex_templates.items():
            regex = re.compile(pattern)
            self.value_regex_templates.append((regex, template))

    def match(self, bucket, key, data):

        if self.bucket:
            if bucket != self.bucket:
                return (False, None)

        if self.prefix:
            if not key.startswith(self.prefix):
                return (False, None)

        for field in self.field:
            if isinstance(data, dict) and field in data:
                data = data[field]
            else:
                return (False, None)

        if data in self.value_templates:
            return (True, self.value_templates[data])

        if isinstance(data, basestring):
            for regex, template in self.value_regex_templates:
                if regex.match(data):
                    return (True, template)

        return (False, None)


def build(routes, templates):

    if not isinstance(routes, list):
        raise TypeError('routes must be a list')

    if not routes:
        raise ValueError('at least 1 route must be defined')

    for route in routes:

        bucket = route.get('bucket')
        prefix = route.get('prefix')

        if 'field' in route:

            field = route['field']

            value_templates = route.get('value', {})
            value_regex_templates = route.get('value_regex', {})

            if not value_templates and not value_regex_templates:
                raise ValueError(
                    'field routes must specify value and/or value_regex'
                )

            field_templates = itertools.chain(
                value_templates.values(),
                value_regex_templates.values(),
            )
            for template in field_templates:
                if template and template not in templates:
                    raise ValueError('unknown template: {}'.format(template))

            yield FieldRoute(
                bucket=bucket,
                prefix=prefix,
                field=field,
                value_templates=value_templates,
                value_regex_templates=value_regex_templates,
            )

        elif 'template' in route:

            template = route['template']

            if template and template not in templates:
                raise ValueError('unknown template: {}'.format(template))

            yield Route(
                bucket=bucket,
                prefix=prefix,
                template=template,
            )

        else:

            raise ValueError('invalid route config: {}'.format(route))
