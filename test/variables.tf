variable "cloudtrail_bucket" {
  type = string
}

variable "elasticsearch_host" {
  type = string
}

variable "elasticsearch_vpc_enabled" {
  type    = bool
  default = false
}

variable "elasticsearch_subnet_ids" {
  type    = list(string)
  default = []
}

variable "elasticsearch_security_group_ids" {
  type    = list(string)
  default = []
}
