# terraform-aws-s3-to-elasticsearch

This module creates a Lambda function that reads files from S3 and indexes them in Elasticsearch. The primary use case is to take logs in S3 and send them to Elasticsearch.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

## Configuration

### Parsers

The purpose of a parser is to define how to read files from S3. Some files contain JSON, others contain lines of JSON, and some are gzipped.

You must define a list of parsers to use for reading files from S3. These are evaluated in order and the first match for a file is used.

Matching works using a key prefix. If the S3 object key starts with the parser's prefix, then it matches. If the parser has no prefix, then every file matches. 

Available parser types are `json`, `ndjson` (newline delimited JSON), and `line`.

### Routes

The purpose of a route is to choose a template for each document being indexed.

You must define a list of routes to use when processing documents. These are evaluated in order and the first match for a document is used.

Matching can be performed using various methods:

* field values (field must match a certain value)
* field_regex values (field must match a certain regular expression)
* default (always use this route)

### Templates

The purpose of a template is to define details of an index and pipeline to use when indexing a document.

You can use the built-in templates or define custom templates. Built-in templates can be found in the `lambda/templates` directory.
