# Create the main Lambda function that ingest documents in S3
# and indexes them in Elasticsearch.

module "ingest_lambda" {
  source = "github.com/claranet/terraform-aws-lambda?ref=v1.2.0"

  function_name                  = random_id.name.hex
  description                    = "Ingests documents from S3 into Elasticsearch"
  handler                        = "main.lambda_handler"
  reserved_concurrent_executions = var.concurrency
  runtime                        = "python3.6"
  timeout                        = var.timeout_seconds
  memory_size                    = var.memory_size

  source_path = "${path.module}/lambda/ingest"

  policy = {
    json = data.aws_iam_policy_document.ingest_lambda.json
  }

  environment = {
    variables = {
      BULK_INDEX_SIZE       = var.bulk_index_size
      CONFIG_BUCKET         = aws_s3_bucket.config.id
      CONFIG_KEY            = aws_s3_bucket_object.config.key
      CONFIG_HASH           = sha256(local.config_json)
      ELASTICSEARCH_HOST    = var.elasticsearch_host
      ELASTICSEARCH_PORT    = var.elasticsearch_port
      ELASTICSEARCH_REGION  = coalesce(var.elasticsearch_region, data.aws_region.current.name)
      ELASTICSEARCH_SIGNING = var.elasticsearch_request_signing ? 1 : 0
      ELASTICSEARCH_SSL     = var.elasticsearch_ssl ? 1 : 0
      ELASTICSEARCH_VERSION = var.elasticsearch_version
      FUNCTION_NAME         = random_id.name.hex
    }
  }

  dead_letter_config = {
    target_arn = aws_sqs_queue.retry.arn
  }

  vpc_config = {
    subnet_ids         = var.vpc_subnet_ids
    security_group_ids = var.vpc_security_group_ids
  }
}

data "aws_iam_policy_document" "ingest_lambda" {
  # Grant access to the config bucket.
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.config.id}/*",
    ]
  }

  # Grant access to the source buckets.
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
    ]

    resources = formatlist("arn:aws:s3:::%v/*", var.bucket_names)
  }

  # Grant access to send metrics.
  statement {
    effect = "Allow"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

# Attach a policy to enable S3 object decryption using KMS keys.

data "aws_iam_policy_document" "kms" {
  count = var.kms_enabled ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "kms:Decrypt",
    ]

    resources = var.kms_key_arns
  }
}

resource "aws_iam_policy" "kms" {
  count = var.kms_enabled ? 1 : 0

  name   = "${var.name}-kms"
  policy = join("", data.aws_iam_policy_document.kms.*.json)
}

resource "aws_iam_policy_attachment" "kms" {
  count = var.kms_enabled ? 1 : 0

  name       = "${var.name}-kms"
  roles      = [module.ingest_lambda.role_name]
  policy_arn = join("", aws_iam_policy.kms.*.arn)
}

# Create the retry Lambda function that consumes failed events
# from the dead letter queue and retries them.

module "retry_lambda" {
  source = "github.com/claranet/terraform-aws-lambda?ref=v1.2.0"

  function_name                  = "${random_id.name.hex}-retry"
  description                    = "Retry ingesting documents from S3 into Elasticsearch"
  handler                        = "retry.lambda_handler"
  reserved_concurrent_executions = 1
  runtime                        = "python3.6"
  timeout                        = 300

  source_path = "${path.module}/lambda/retry.py"

  policy = {
    json = data.aws_iam_policy_document.retry_lambda.json
  }

  environment = {
    variables = {
      FAILED_BUCKET        = aws_s3_bucket.config.id
      FAILED_PREFIX        = "failed"
      FUNCTION_NAME        = "${random_id.name.hex}-retry"
      INGEST_FUNCTION_NAME = module.ingest_lambda.function_name
      RETRY_BACKOFF        = jsonencode(var.retry_backoff)
      RETRY_QUEUE_URL      = aws_sqs_queue.retry.id
    }
  }
}

data "aws_iam_policy_document" "retry_lambda" {
  # Grant access to the retry queue.
  statement {
    effect = "Allow"

    actions = [
      "sqs:DeleteMessage*",
      "sqs:ReceiveMessage",
      "sqs:SendMessage*",
    ]

    resources = [
      aws_sqs_queue.retry.arn,
    ]
  }

  # Grant access to invoke the ingest Lambda function.
  statement {
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction",
    ]

    resources = [
      module.ingest_lambda.function_arn,
    ]
  }

  # Grant access to the failed bucket.
  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.config.id}/failed/*",
    ]
  }

  # Grant access to send metrics.
  statement {
    effect = "Allow"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

