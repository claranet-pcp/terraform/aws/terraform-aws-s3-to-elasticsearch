import certifi
import cloudwatch
import config
import json
import os
import s3
import time
import urllib

from aws_requests_auth.aws_auth import AWSRequestsAuth
from aws_requests_auth import boto_utils

from botocore.utils import datetime2timestamp, parse_timestamp

from collections import deque


BULK_INDEX_SIZE = int(os.environ['BULK_INDEX_SIZE'])

CONFIG_BUCKET = os.environ['CONFIG_BUCKET']
CONFIG_KEY = os.environ['CONFIG_KEY']

ELASTICSEARCH_HOST = os.environ['ELASTICSEARCH_HOST']
ELASTICSEARCH_PORT = int(os.environ['ELASTICSEARCH_PORT'])
ELASTICSEARCH_REGION = os.environ['ELASTICSEARCH_REGION']
ELASTICSEARCH_SIGNING = bool(int(os.environ['ELASTICSEARCH_SIGNING']))
ELASTICSEARCH_SSL = bool(int(os.environ['ELASTICSEARCH_SSL']))
ELASTICSEARCH_VERSION = os.environ['ELASTICSEARCH_VERSION']

FUNCTION_NAME = os.environ['FUNCTION_NAME']

RETRY_DETAILS_KEY = 'LambdaIngestRetryDetails'
RETRY_DEQUE = deque()
RETRY_SET = set()

INDEX_TEMPLATE_CACHE = set()
PIPELINE_CACHE = set()


if ELASTICSEARCH_VERSION == '5':
    from elasticsearch5 import Elasticsearch, RequestsHttpConnection
    from elasticsearch5.helpers import bulk, BulkIndexError
elif ELASTICSEARCH_VERSION == '6' or ELASTICSEARCH_VERSION == '7':
    from elasticsearch import Elasticsearch, RequestsHttpConnection
    from elasticsearch.helpers import bulk, BulkIndexError
else:
    raise ValueError('unsupported elasticsearch version: {}'.format(
        ELASTICSEARCH_VERSION
    ))


# Load the config data from S3 when the Lambda process starts.
with s3.open_object(CONFIG_BUCKET, CONFIG_KEY) as open_file:
    config_data = json.load(open_file)
    config.build(ELASTICSEARCH_VERSION, **config_data)


def chunks(items, size):
    """
    Splits an iterable into lists of a certain size.

    """

    out = []
    try:
        while True:
            for _ in range(size):
                out.append(next(items))
            yield out
            out = []
    except StopIteration:
        if out:
            yield out


def elasticsearch_client():
    """
    Returns an Elasticsearch client.

    """

    kwargs = {
        'host': ELASTICSEARCH_HOST,
        'port': ELASTICSEARCH_PORT,
    }

    if ELASTICSEARCH_SSL:
        kwargs.update({
            'use_ssl': True,
            'ca_certs': certifi.where(),
        })

    if ELASTICSEARCH_SIGNING:
        kwargs.update({
            'connection_class': RequestsHttpConnection,
            'http_auth': AWSRequestsAuth(
                aws_host=ELASTICSEARCH_HOST,
                aws_region=ELASTICSEARCH_REGION,
                aws_service='es',
                **boto_utils.get_credentials()
            ),
        })

    return Elasticsearch(**kwargs)


def generate_index_actions(es, docs, bucket, key, timestamp):

    for line_number, doc in enumerate(docs, start=1):

        # Create an ID that is unique but repeatable,
        # to avoid duplicate documents when reindexing files.
        doc_id = '{}/{}:{}'.format(bucket, key, line_number)

        # Use the routes configuration to determine which template to use
        # for this document.
        for route in config.ROUTES:
            matched, template_name = route.match(bucket, key, doc)
            if matched:
                if template_name:
                    template = config.TEMPLATES[template_name]
                else:
                    template = None
                break
        else:
            raise ValueError('no matching route for {}'.format(doc_id))

        if not template:
            continue

        # Add ingest details to the document.
        doc['_ingest'] = {
            'timestamp': timestamp.isoformat(timespec='milliseconds'),
        }

        # Build the action data for the bulk API call.
        action = {
            '_op_type': 'index',
            '_index': template.get_index(timestamp),
            '_type': template_name,
            '_id': doc_id,
            '_source': doc,
        }

        # Make a unique name for index templates and pipelines.
        template_id = '{}-{}'.format(FUNCTION_NAME, template_name)

        # Use a pipeline if configured.
        if template.pipeline:
            put_pipeline(es, template_id, template.pipeline)
            action['pipeline'] = template_id

        # Use an index template if configured.
        if template.template:
            put_index_template(es, template_id, template.template)

        yield action


def put_index_template(es, name, template):
    """
    Creates or replaces an index template in Elasticsearch.

    """

    if name not in INDEX_TEMPLATE_CACHE:

        print('DEBUG Put index template {}'.format(name))

        response = es.indices.put_template(name, template)
        if not response['acknowledged']:
            raise Exception('ERROR: {}'.format(response))

        INDEX_TEMPLATE_CACHE.add(name)


def put_pipeline(es, name, pipeline):
    """
    Creates or replaces a pipeline in Elasticsearch.

    """

    if name not in PIPELINE_CACHE:

        print('DEBUG Put pipeline {}'.format(name))

        # Fix types.
        for processor in pipeline.get('processors', []):
            for settings in processor.values():
                ignore_missing = settings.get('ignore_missing')
                if ignore_missing == '1':
                    settings['ignore_missing'] = True
                elif ignore_missing == '0':
                    settings['ignore_missing'] = False

        response = es.ingest.put_pipeline(name, pipeline)
        if not response['acknowledged']:
            raise Exception('ERROR: {}'.format(response))

        PIPELINE_CACHE.add(name)


def process_s3_object(bucket, key):
    """
    Downloads an object from S3, parses it for documents,
    and indexes them in Elasticsearch.

    This function is a generator that yields the number of index operations
    in each bulk API request sent to Elasticsearch. The total sum of these
    numbers is equal to the number of documents found in the S3 object.

    """

    # Get data about the object in S3.
    response = s3.get_object(Bucket=bucket, Key=key)
    timestamp = response['LastModified']

    # Choose the parser for this object.
    for parser in config.PARSERS:
        if parser.match(bucket, key):
            break
    else:
        raise Exception('no parser matched')

    # Open and parse the documents in the S3 object.
    with parser.download_and_parse(bucket, key) as docs:

        # Make an Elasticsearch client.
        es = elasticsearch_client()

        # Create a stream of index actions.
        index_actions = generate_index_actions(
            es=es,
            docs=docs,
            bucket=bucket,
            key=key,
            timestamp=timestamp,
        )

        # Process the actions in bulk. If there are issues indexing
        # documents, try to keep going so that one document doesn't
        # prevent others from being indexed.

        soft_error = None

        for actions in chunks(index_actions, BULK_INDEX_SIZE):

            action_count = len(actions)

            print('DEBUG Indexing {} +{}'.format(
                actions[0]['_id'], action_count,
            ))

            try:
                bulk(
                    client=es,
                    actions=actions,
                    chunk_size=BULK_INDEX_SIZE,
                    raise_on_error=True,
                    raise_on_exception=True,
                )
            except BulkIndexError as bulk_index_error:
                # Check if the errors were related to pipeline processors,
                # e.g. a grok filter didn't match. This means that there
                # is a problem specific to the pipeline and/or the document
                # it was trying to process, so there is a chance that other
                # documents can still be indexed. Try to detect these types
                # of errors by looking for the following data structure:
                # { index: { error: { header: { processor_type: name } } }
                for error in bulk_index_error.errors:
                    error = error['index']['error']
                    if 'header' not in error:
                        # This does not seem to be a pipeline error,
                        # so raise it immediately.
                        raise
                    elif 'processor_type' not in error['header']:
                        # This does not seem to be a pipeline error,
                        # so raise it immediately.
                        raise
                    else:
                        # This seems to be a pipeline error, so keep going.
                        pass
                else:
                    # All of the errors appeared to be pipeline errors,
                    # so save it for later and keep running.
                    print('WARNING {}'.format(bulk_index_error.args[0]))
                    soft_error = bulk_index_error

            # Yield the number of documents sent to Elasticsearch.
            yield action_count

        # If there was an indexing error that wasn't raised immediately,
        # raise it now so the function fails and it can be retried later.
        if soft_error:
            raise soft_error


def lambda_handler(event, context):
    """
    Handles an S3 bucket notification event (or a fake event with the same
    structure) to process an S3 object, parse it for documents and index
    them in Elasticsearch.

    """

    invocation_time = time.time()

    # Get the file details from the S3 bucket notification event.
    record = event['Records'][0]
    bucket = record['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(record['s3']['object']['key'])
    url = 's3://{}/{}'.format(bucket, key)
    run_count = event.get(RETRY_DETAILS_KEY, {}).get('run', 1)
    print('INFO Processing ({}) {}'.format(run_count, url))

    # Skip duplicate invocations. Lambda automatically retries failed events
    # but this function has a dead letter queue with its own retry logic.
    event_time = record['eventTime']
    retry_key = (bucket, key, event_time, run_count)
    if retry_key in RETRY_SET:
        raise Exception('DEBUG Skipping duplicate event for {}'.format(url))
    RETRY_DEQUE.appendleft(retry_key)
    RETRY_SET.add(retry_key)
    if len(RETRY_SET) > 200:
        RETRY_SET.remove(RETRY_DEQUE.pop())

    # Process the S3 object and keep track of the number of documents
    # that were sent to Elasticsearch.
    index_operations = 0
    try:
        for action_count in process_s3_object(bucket, key):
            index_operations += action_count
    finally:

        # Prepare to send CloudWatch metrics.
        metric_data = []
        metric_dimensions = [
            {
                'Name': 'FunctionName',
                'Value': FUNCTION_NAME,
            }
        ]

        # Add metrics for tracking the number of documents sent to
        # Elasticsearch, in other words the throughput of this function.
        metric_data.append({
            'MetricName': 'IndexOperations',
            'Dimensions': metric_dimensions,
            'Value': index_operations,
            'Unit': 'Count',
        })

        # Add metrics for the time between new S3 bucket notification
        # events and when Lambda is invoked with them. This is the lag
        # time, showing whether the Lambda function is keeping up with
        # the S3 bucket uploads. If the lag time is high, there may be
        # an issue, or the Lambda function's concurrency might need to
        # be increased.
        if run_count == 1:
            event_timestamp = datetime2timestamp(parse_timestamp(event_time))
            lag_value = invocation_time - event_timestamp
            metric_data.append({
                'MetricName': 'Lag',
                'Dimensions': metric_dimensions,
                'Value': lag_value,
                'Unit': 'Seconds',
            })

        # Send the above metrics to CloudWatch.
        cloudwatch.put_metric_data(
            Namespace='Ingest',
            MetricData=metric_data,
        )
