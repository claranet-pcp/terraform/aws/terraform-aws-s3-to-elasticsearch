# Resource names

variable "name" {
  description = "The base name used for created resources"
  type        = string
}

variable "random_name_byte_length" {
  description = "The byte length of the random id generator used for unique resource names"
  default     = 4
}

# Source buckets

variable "bucket_count" {
  description = "Number of S3 buckets to ingest from"
  type        = string
}

variable "bucket_names" {
  description = "List of S3 buckets to ingest from"
  type        = list(string)
}

variable "kms_enabled" {
  description = "Add a policy for decrypting S3 objects with KMS keys"
  type        = string
  default     = false
}

variable "kms_key_arns" {
  description = "List of KMS key ARNs for decrypting S3 objects"
  type        = list(string)
  default     = []
}

# Elasticsearch server

variable "elasticsearch_host" {
  type = string
}

variable "elasticsearch_port" {
  default = 9200
}

variable "elasticsearch_ssl" {
  default = false
}

variable "elasticsearch_version" {
  description = "The major version of the Elasticsearch server (5 or 6)"
  type        = string
}

variable "elasticsearch_request_signing" {
  description = "Enable this to sign Amazon ES requests"
  default     = false
}

variable "elasticsearch_region" {
  description = "AWS region to use when signing Amazon ES requests (defaults to current region)"
  default     = ""
}

variable "vpc_enabled" {
  default = false
}

variable "vpc_subnet_ids" {
  default = []
}

variable "vpc_security_group_ids" {
  default = []
}

# Performance tweaking

variable "concurrency" {
  default = 1
}

variable "bulk_index_size" {
  default = 1000
}

variable "retry_backoff" {
  # default value fails after ~ 1 hour
  description = "Retry backoff sequence (minutes to wait before retrying, max 15 each)"
  default     = [5, 10, 15, 15, 15]
}

variable "timeout_seconds" {
  description = "Maximum time for the ingest Lambda function to run per file"
  default     = 60
}

variable "memory_size" {
  description = "Memory to assign to the ingest Lambda function"
  default     = 128
}

# Indexing configuration

variable "parsers" {
  type = list(any)
}

variable "routes" {
  type = list(any)
}

variable "templates" {
  type = any
}

# Alarm configuration

variable "info_alarm_actions" {
  description = "Alarm actions for INFO level CloudWatch alarms (ingestion attempt failed)"
  type        = list(string)
  default     = []
}

variable "info_ok_actions" {
  description = "OK actions for INFO level CloudWatch alarms"
  type        = list(string)
  default     = []
}

variable "warning_alarm_actions" {
  description = "Alarm actions for WARNING level CloudWatch alarms (increased lag)"
  type        = list(string)
  default     = []
}

variable "warning_ok_actions" {
  description = "OK actions for WARNING level CloudWatch alarms"
  type        = list(string)
  default     = []
}

variable "error_alarm_actions" {
  description = "Alarm actions for ERROR level CloudWatch alarms (files could not be ingested)"
  type        = list(string)
  default     = []
}

variable "error_ok_actions" {
  description = "OK actions for ERROR level CloudWatch alarms"
  type        = list(string)
  default     = []
}

variable "critical_alarm_actions" {
  description = "Alarm actions for CRITICAL level CloudWatch alarms (retry function and DLQ errors, invalid events)"
  type        = list(string)
  default     = []
}

variable "critical_ok_actions" {
  description = "OK actions for CRITICAL level CloudWatch alarms"
  type        = list(string)
  default     = []
}

