import json
import os

import config_parsers
import config_routes
import config_templates


TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')

PARSERS = []
ROUTES = []
TEMPLATES = {}


def build(version, parsers, routes, templates):

    # Load built-in templates.

    for file_name in os.listdir(TEMPLATE_DIR):
        template_path = os.path.join(TEMPLATE_DIR, file_name)
        template_name = os.path.splitext(file_name)[0]
        if template_name not in templates:
            with open(template_path) as open_file:
                templates[template_name] = json.load(open_file)

    # Handle templates with (es5) and (es6) keys.

    if not isinstance(version, int):
        version = int(version)

    if version == 5:
        keep_field = '(es5)'
        remove_field = '(es6)'
    elif version == 6 or version == 7:
        keep_field = '(es6)'
        remove_field = '(es5)'
    else:
        raise ValueError(
            'unsupported elasticsearch version {}'.format(version)
        )

    replace_versioned_fields(
        keep=keep_field,
        remove=remove_field,
        data=[parsers, routes, templates]
    )

    # Build the different config objects and update the global variables.

    TEMPLATES.update(config_templates.build(version, templates))
    PARSERS.extend(config_parsers.build(parsers, TEMPLATES))
    ROUTES.extend(config_routes.build(routes, TEMPLATES))


def replace_versioned_fields(keep, remove, data):
    if isinstance(data, list):
        for value in data:
            replace_versioned_fields(keep, remove, value)
    elif isinstance(data, dict):
        for key in tuple(data.keys()):
            if key.endswith(keep):
                new_key = key[:-len(keep)]
                data[new_key] = data.pop(key)
            elif key.endswith(remove):
                del data[key]
        for value in data.values():
            replace_versioned_fields(keep, remove, value)
