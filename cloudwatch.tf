# Schedule the retry Lambda function to run every 5 minutes.

resource "aws_cloudwatch_event_rule" "retry" {
  name                = module.retry_lambda.function_name
  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "retry" {
  target_id = module.retry_lambda.function_name
  rule      = aws_cloudwatch_event_rule.retry.name
  arn       = module.retry_lambda.function_arn
}

resource "aws_lambda_permission" "retry" {
  statement_id  = module.retry_lambda.function_name
  action        = "lambda:InvokeFunction"
  function_name = module.retry_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.retry.arn
}

# CloudWatch dashboard.

data "template_file" "dashboard" {
  template = file("${path.module}/dashboard.json.tpl")

  vars = {
    ingest_function_name = module.ingest_lambda.function_name
    retry_function_name  = module.retry_lambda.function_name
    region               = data.aws_region.current.name
  }
}

resource "aws_cloudwatch_dashboard" "dashboard" {
  dashboard_name = module.ingest_lambda.function_name
  dashboard_body = data.template_file.dashboard.rendered
}

# CloudWatch alarms.

# Ingest errors are probably bad, but if it is a transient network issue then
# it would retry and succeed. They are still probably worth looking at.

resource "aws_cloudwatch_metric_alarm" "ingest_errors" {
  alarm_name        = "${module.ingest_lambda.function_name}-errors"
  alarm_description = "${module.ingest_lambda.function_name} invocation errors"

  namespace   = "AWS/Lambda"
  metric_name = "Errors"

  dimensions = {
    FunctionName = module.ingest_lambda.function_name
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 900
  evaluation_periods  = 1

  alarm_actions = var.info_alarm_actions
  ok_actions    = var.info_ok_actions
}

# Retry errors are very bad.

resource "aws_cloudwatch_metric_alarm" "retry_errors" {
  alarm_name        = "${module.retry_lambda.function_name}-errors"
  alarm_description = "${module.retry_lambda.function_name} invocation errors"

  namespace   = "AWS/Lambda"
  metric_name = "Errors"

  dimensions = {
    FunctionName = module.retry_lambda.function_name
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 900
  evaluation_periods  = 1

  alarm_actions = var.critical_alarm_actions
  ok_actions    = var.critical_ok_actions
}

# Dead letter errors are very bad.
# > If for some reason, the event payload consistently fails to reach the
# > target ARN, Lambda increments a CloudWatch metric called DeadLetterErrors
# > and then deletes the event payload.

resource "aws_cloudwatch_metric_alarm" "dlq_errors" {
  alarm_name        = "${module.ingest_lambda.function_name}-dlq-errors"
  alarm_description = "${module.ingest_lambda.function_name} dead letter errors"

  namespace   = "AWS/Lambda"
  metric_name = "DeadLetterErrors"

  dimensions = {
    FunctionName = module.ingest_lambda.function_name
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 900
  evaluation_periods  = 1
  treat_missing_data  = "notBreaching"

  alarm_actions = var.critical_alarm_actions
  ok_actions    = var.critical_ok_actions
}

# Invalid messages are very bad.

resource "aws_cloudwatch_metric_alarm" "invalid_messages" {
  alarm_name        = "${module.retry_lambda.function_name}-invalid-messages"
  alarm_description = "${module.retry_lambda.function_name} invalid messages"

  namespace   = "Ingest"
  metric_name = "InvalidMessages"

  dimensions = {
    FunctionName = module.retry_lambda.function_name
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 900
  evaluation_periods  = 1
  treat_missing_data  = "notBreaching"

  alarm_actions = var.critical_alarm_actions
  ok_actions    = var.critical_ok_actions
}

# Failed files are S3 files that could not be ingested.
# Example reasons:
#   * Lambda unable to parse file
#   * Ingest pipeline processor failure (e.g. Grok filter not matching)
#   * Elasticsearch server is broken

resource "aws_cloudwatch_metric_alarm" "failed_files" {
  alarm_name        = "${module.retry_lambda.function_name}-failed-files"
  alarm_description = "${module.retry_lambda.function_name} failed files"

  namespace   = "Ingest"
  metric_name = "FailedFiles"

  dimensions = {
    FunctionName = module.retry_lambda.function_name
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 900
  evaluation_periods  = 1
  treat_missing_data  = "notBreaching"

  alarm_actions = var.error_alarm_actions
  ok_actions    = var.error_ok_actions
}

# Lag is the time taken for a new S3 bucket event notification to be sent to
# the Ingest Lambda function. If the function is being throttled, then the lag
# will be high. Lag is not necessarily a bad thing if it is generally low but
# with some spikes. If it is consistently high then there is a problem.
# Example reasons:
#   * There was a spike in the amount of log data to to process
#   * Elasticsearch is slow and cannot keep up
#   * The Ingest Lambda function's concurrency is too low

resource "aws_cloudwatch_metric_alarm" "lag" {
  alarm_name        = "${module.ingest_lambda.function_name}-lag"
  alarm_description = "${module.ingest_lambda.function_name} lag"

  namespace   = "Ingest"
  metric_name = "Lag"

  dimensions = {
    FunctionName = module.ingest_lambda.function_name
  }

  statistic           = "Average"
  comparison_operator = "GreaterThanThreshold"
  threshold           = var.timeout_seconds
  period              = 60
  evaluation_periods  = 5
  treat_missing_data  = "notBreaching"

  alarm_actions = var.warning_alarm_actions
  ok_actions    = var.warning_ok_actions
}

