data "external" "validate" {
  program = ["${path.module}/validate.py"]

  query = {
    config                = local.config_json
    elasticsearch_version = var.elasticsearch_version
  }
}

