import boto3
import codecs
import contextlib
import tempfile


s3 = boto3.client('s3')


@contextlib.contextmanager
def open_object(bucket, key, mode='r'):
    with tempfile.NamedTemporaryFile() as temp_file:
        s3.download_file(bucket, key, temp_file.name)
        with open(temp_file.name, mode) as open_file:
            yield open_file


def get_object(**kwargs):
    response = s3.get_object(**kwargs)
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))
    return response


def stream_object(bucket, key):
    s3_object = s3.get_object(
        Bucket=bucket,
        Key=key,
    )
    body = s3_object['Body']
    reader = codecs.getreader('utf-8')
    return reader(body)
