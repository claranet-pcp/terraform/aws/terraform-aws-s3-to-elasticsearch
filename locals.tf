locals {
  config = {
    parsers   = var.parsers
    routes    = var.routes
    templates = var.templates
  }

  config_json = jsonencode(local.config)
}

