import boto3
import datetime
import json
import os
import uuid


AWS_MAX_DELAY_SECONDS = 900  # 15 minutes, hard limit of the API

FAILED_BUCKET = os.environ['FAILED_BUCKET']
FAILED_PREFIX = os.environ['FAILED_PREFIX']
FUNCTION_NAME = os.environ['FUNCTION_NAME']
INGEST_FUNCTION_NAME = os.environ['INGEST_FUNCTION_NAME']
RETRY_BACKOFF = tuple(
    min(int(minutes) * 60, AWS_MAX_DELAY_SECONDS)
    for minutes in json.loads(os.environ['RETRY_BACKOFF'])
)
RETRY_QUEUE_URL = os.environ['RETRY_QUEUE_URL']

RETRY_DETAILS_KEY = 'LambdaIngestRetryDetails'


cloudwatch = boto3.client('cloudwatch')
lambda_client = boto3.client('lambda')
s3 = boto3.client('s3')
sqs = boto3.client('sqs')


def get_messages():
    """
    Gets up to 10 messages from the dead letter queue.

    """

    response = sqs.receive_message(
        QueueUrl=RETRY_QUEUE_URL,
        MaxNumberOfMessages=10,
        VisibilityTimeout=600,
        WaitTimeSeconds=0,
        MessageAttributeNames=['All'],
    )

    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))

    return response.get('Messages', [])


def lambda_invoke(payload):
    """
    Invokes the ingest Lambda function with the given payload.

    """

    response = lambda_client.invoke(
        FunctionName=INGEST_FUNCTION_NAME,
        InvocationType='Event',
        Payload=payload,
    )
    if response['StatusCode'] != 202:
        raise Exception('ERROR: {}'.format(response))


def put_metric_data(**kwargs):
    """
    Publishes metric data to Amazon CloudWatch.

    """

    response = cloudwatch.put_metric_data(**kwargs)
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))


def sqs_delete_message_batch(entries):
    """
    Deletes consumed messages from the queue.
    """

    response = sqs.delete_message_batch(
        QueueUrl=RETRY_QUEUE_URL,
        Entries=entries,
    )
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))
    if response.get('Failed'):
        raise Exception('ERROR: {}'.format(response))


def sqs_send_message_batch(entries):
    """
    Puts messages onto the queue.

    """

    response = sqs.send_message_batch(
        QueueUrl=RETRY_QUEUE_URL,
        Entries=entries,
    )
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))
    if response.get('Failed'):
        raise Exception('ERROR: {}'.format(response))


def write_failed(failed):
    """
    Writes details of failed files to S3.

    """

    key = datetime.datetime.now().strftime(
        '{prefix}/%Y/%m/%d/%H/%H-%M-%S-{uuid}.json'.format(
            prefix=FAILED_PREFIX,
            uuid=uuid.uuid1().hex,
        ),
    )

    print('INFO Writing s3://{}/{}'.format(FAILED_BUCKET, key))

    response = s3.put_object(
        Bucket=FAILED_BUCKET,
        Key=key,
        Body=json.dumps(failed, indent=2).encode('utf-8')
    )
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('ERROR: {}'.format(response))


def lambda_handler(event, context):
    """
    Consumes messages from the ingest Lambda function's dead letter queue
    and retries them, respecting the backoff sequence. If the ingest function
    fails to process a file too many times, the file details are written to
    a JSON file in the failed bucket.

    """

    invalid_count = 0
    failed_count = 0
    retried_count = 0

    while True:

        # Stop if the Lambda function is about to time out.
        if context.get_remaining_time_in_millis() < 1000 * 30:
            break

        # Get messages from the dead letter queue.
        messages = get_messages()

        # Stop when the queue is empty.
        if not messages:
            break

        # Prepare for batch operations.
        failed = []
        lambda_invoke_payloads = []
        sqs_delete_message_batch_entries = []
        sqs_send_message_batch_entries = []

        seen = set()

        # Handle each message.
        for message in messages:

            # Prepare to delete this message after the loop ends.
            sqs_delete_message_batch_entries.append({
                'Id': message['MessageId'],
                'ReceiptHandle': message['ReceiptHandle'],
            })

            # Parse the event from the message.
            try:
                event = json.loads(message['Body'])
                record = event['Records'][0]
                bucket = record['s3']['bucket']['name']
                key = event['Records'][0]['s3']['object']['key']
                record['eventTime']
            except KeyError:
                print('CRITICAL Invalid message', message)
                invalid_count += 1
                continue

            url = 's3://{}/{}'.format(bucket, key)

            # Discard duplicate events for the same file.
            if url in seen:
                continue
            else:
                seen.add(url)

            # Get the retry details from the event. If this is the first
            # failure, the details will be missing. In that case, we already
            # know that it has run once (and failed) and not waited yet.
            if RETRY_DETAILS_KEY not in event:
                event[RETRY_DETAILS_KEY] = {'run': 1, 'wait': 0}
            run_count = event[RETRY_DETAILS_KEY]['run']
            wait_count = event[RETRY_DETAILS_KEY]['wait']

            if run_count >= len(RETRY_BACKOFF):

                # It ran too many times, give up on processing this file
                # and add it to the "failed" S3 bucket.

                print('INFO Failed {}'.format(url))
                failed.append({
                    'Bucket': bucket,
                    'Key': key,
                })

            elif run_count > wait_count:

                # It just ran. Increment the wait count and put the event
                # back on the dead letter queue but with a delay.

                # When it gets consumed again, the wait count will equal the
                # run count and it will run it again.

                print('INFO Delay ({}, {}m) {}'.format(
                    wait_count + 1, RETRY_BACKOFF[wait_count], url,
                ))

                event[RETRY_DETAILS_KEY]['wait'] += 1

                sqs_send_message_batch_entries.append({
                    'Id': message['MessageId'],
                    'MessageBody': json.dumps(event, indent=2),
                    'DelaySeconds': RETRY_BACKOFF[wait_count],
                })

            else:

                # It just waited. Increment the run count and run it again.

                # If it fails again, it will appear back in the queue with
                # the run count and wait count being equal. It will then
                # either wait again or give up if it has run too many times.

                print('INFO Run ({}) {}'.format(run_count + 1, url))

                event[RETRY_DETAILS_KEY]['run'] += 1

                lambda_invoke_payloads.append(json.dumps(event, indent=2))

        # Events that have retried and failed too many times
        # are put into the "failed" S3 bucket.
        failed_count += len(failed)
        if failed:
            write_failed(failed)

        # Retry events that have just waited and should now run.
        retried_count += len(lambda_invoke_payloads)
        for payload in lambda_invoke_payloads:
            lambda_invoke(payload)

        # Send messages for events that just ran and should now wait.
        if sqs_send_message_batch_entries:
            sqs_send_message_batch(sqs_send_message_batch_entries)

        # Delete processed messages.
        sqs_delete_message_batch(sqs_delete_message_batch_entries)

    # Send metrics to CloudWatch.
    metric_data = []
    counts = {
        'InvalidMessages': invalid_count,
        'FailedFiles': failed_count,
        'RetriedFiles': retried_count,
    }
    for metric_name, value in counts.items():
        metric_data.append({
            'MetricName': metric_name,
            'Dimensions': [
                {
                    'Name': 'FunctionName',
                    'Value': FUNCTION_NAME,
                }
            ],
            'Value': value,
            'Unit': 'Count',
        })
    put_metric_data(
        Namespace='Ingest',
        MetricData=metric_data,
    )
