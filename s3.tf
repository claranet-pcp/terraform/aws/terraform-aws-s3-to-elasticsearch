# Upload the configuration to S3 rather than using Lambda environment
# variables because they have a 4kb maximum size.

resource "aws_s3_bucket" "config" {
  bucket = random_id.name.hex
  acl    = "private"
}

resource "aws_s3_bucket_object" "config" {
  bucket  = aws_s3_bucket.config.id
  key     = "config.json"
  content = local.config_json
}

# Invoke the Lambda function on S3 bucket uploads.

resource "aws_lambda_permission" "allow_bucket" {
  count         = var.bucket_count
  statement_id  = "AllowExecutionS3-${element(var.bucket_names, count.index)}"
  action        = "lambda:InvokeFunction"
  function_name = module.ingest_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${element(var.bucket_names, count.index)}"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  count      = var.bucket_count
  bucket     = element(var.bucket_names, count.index)
  depends_on = [aws_lambda_permission.allow_bucket]

  lambda_function {
    lambda_function_arn = module.ingest_lambda.function_arn
    events              = ["s3:ObjectCreated:*"]
  }
}

