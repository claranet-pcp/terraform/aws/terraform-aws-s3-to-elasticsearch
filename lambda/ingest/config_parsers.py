import contextlib
import gzip
import json
import s3


class Parser(object):

    def __init__(self, bucket, prefix, gzip, stream):
        self.bucket = bucket
        self.prefix = prefix
        self.gzip = gzip
        self.stream = stream

    @contextlib.contextmanager
    def download_and_parse(self, bucket, key):

        if self.stream:

            open_stream = s3.stream_object(bucket, key)
            yield self.parse(open_stream=open_stream)

        else:

            if self.gzip:
                file_mode = 'rb'
            else:
                file_mode = 'r'

            with s3.open_object(bucket, key, file_mode) as open_file:
                with self.extract(open_file) as extracted_file:
                    yield self.parse(open_file=extracted_file)

    @contextlib.contextmanager
    def extract(self, open_file):
        if self.gzip:
            with gzip.open(open_file) as extracted_file:
                yield extracted_file
        else:
            yield open_file

    def match(self, bucket, key):

        if self.bucket:
            if bucket != self.bucket:
                return False

        if self.prefix:
            if not key.startswith(self.prefix):
                return False

        return True

    def parse(self, open_file=None, open_stream=None):
        raise NotImplementedError('subclasses must implement parse')


class JSONParser(Parser):
    """
    JSON filed parser with optional field access.

    """

    def __init__(self, field, **kwargs):

        super(JSONParser, self).__init__(**kwargs)

        if not field:
            self.field = []
        elif isinstance(field, list):
            self.field = field
        else:
            self.field = [field]

    def parse(self, open_file=None, open_stream=None):

        if open_file:
            data = json.load(open_file)
        else:
            data = json.loads(str(open_stream))

        for field in self.field:
            try:
                data = data[field]
            except Exception:
                raise ValueError(
                    'error accessing fields: {}'.format(self.field)
                )

        if not isinstance(data, list):
            raise TypeError('no list in fields: {}'.format(self.field))

        return data


class NDJSONParser(Parser):
    """
    Newline delimited JSON file parser.

    """

    def parse(self, open_file=None, open_stream=None):

        lines = open_file or open_stream
        for line in lines:
            yield json.loads(line)

class LineParser(Parser):
    """
    Newline delimited file parser.

    """

    def parse(self, open_file=None, open_stream=None):

        lines = open_file or open_stream
        for line in lines:
            yield json.loads(json.dumps({'message': line.decode('utf-8')}))


def build(parsers, templates):

    if not isinstance(parsers, list):
        raise TypeError('parsers must be a list')

    if not parsers:
        raise ValueError('at least 1 parser must be defined')

    for parser in parsers:

        bucket = parser.get('bucket')
        prefix = parser.get('prefix')

        template = parser.get('template')
        if template:
            if template not in templates:
                raise ValueError('unknown template: {}'.format(template))
            if not templates[template].parser:
                raise ValueError('no parser in template: {}'.format(template))
            parser = templates[template].parser

        gzip = parser.get('gzip')
        stream = parser.get('stream')
        if gzip and stream:
            raise ValueError('gzip and stream cannot be used at the same time')

        parser_type = parser.get('type')
        if parser_type == 'json':
            field = parser.get('field')
            yield JSONParser(
                bucket=bucket,
                prefix=prefix,
                gzip=gzip,
                stream=stream,
                field=field,
            )
        elif parser_type == 'ndjson':
            yield NDJSONParser(
                bucket=bucket,
                prefix=prefix,
                gzip=gzip,
                stream=stream,
            )
        elif parser_type == 'line':
            yield LineParser(
                bucket=bucket,
                prefix=prefix,
                gzip=gzip,
                stream=stream,
            )
        else:
            raise ValueError('invalid parser type: {}'.format(parser_type))
