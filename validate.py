#!/usr/bin/env python3

import json
import os
import sys

sys.dont_write_bytecode = True

script_dir = os.path.abspath(os.path.dirname(__file__))
lambda_dir = os.path.join(script_dir, 'lambda', 'ingest')
sys.path.insert(0, lambda_dir)

import config

# Read the query in from Terraform. It passes in the same JSON string
# as the config S3 object that is used by the Lambda function.
query = json.load(sys.stdin)
config_data = json.loads(query['config'])
elasticsearch_version = query['elasticsearch_version']

# Build the configs to ensure they are valid.
config.build(elasticsearch_version, **config_data)

# Output the result back to Terraform.
json.dump({'valid': "1"}, sys.stdout, indent=2)
sys.stdout.write('\n')
